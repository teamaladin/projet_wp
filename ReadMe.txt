﻿PROJET WORDPRESS

But du projet : Créer un site à base de Wordpress pour la gazette "Haut de Garonne"

Il doit :
- Etre possible d'ajouter un ou plusieurs articles aux différentes pages du site avec une date d'ajout.
- Y avoir de la place libre pour laisser la possibilité d'insérer des publicités qui sont un moyen de 
	financement pour la gazette.
- Etre possible d'y laisser des commentaires (qui doivent être préalablement validé par un 	     
    administrateur).

 Le site devra également contenir d'autres pages notamment les pages "Mentions légales", "A Propos"et "Formulaire Contact".



Membres de l'équipe :
- AUGER Nicolas
- CLUZEL Adrien(dieu)
- POZZER Lucas
- LABORDE Come


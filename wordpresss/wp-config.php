<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'wordpress');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');
/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'IF[|:]KzR0f!n,Ch-gUY_5hQnUFi15)C=^9yj+rD)8^&YWz<WeqyNHUDn~Y[R+@N');
define('SECURE_AUTH_KEY',  'Gy+zCZPNT*B2zu|@~H~5f)*~-L71X(r0&/4mJ#LcF#-pQJqnDH.FLw+l&mRew`dO');
define('LOGGED_IN_KEY',    'SV@`l*frV%f-ETW%2I+Dm$9N7)IReS?TJCrdyZ|yl|5oUrHEg>6fyfgM,[L+1wo#');
define('NONCE_KEY',        'Z?YCAmVCA--l&Fy(cjhqYy^]`o[9VtU= 6.AA_BREi(Z2fp`[>Ps`NUss$^Q+&wq');
define('AUTH_SALT',        'rWq^~!~|p:V$4ms$b&oA{ (!g&dCDs.9$;<W95k)~dI+||vov/xv4-+ 6HY|9^yQ');
define('SECURE_AUTH_SALT', 'x8Yrt tj@}TIX5lp+8QiGt_;3zu|`+-Vx_}kFf$~zF+F_a<y24 I_NB7B2k}`)f5');
define('LOGGED_IN_SALT',   '3/[G^j(Z*HWN?h?v>s4B$ORa!,H, LlseEHf.a::k705%u0@wv++dxNCSvD7+:Hk');
define('NONCE_SALT',       'Yq4gBEn fPP!o,~5;,F/RNJ0P);sl?0_MZi8R6~kN7vX-#6T0;f(/+rqi<wXvJT.');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

define ('WP_ALLOW_MULTISITE', true);

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
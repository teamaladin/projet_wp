<?php 

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}



function projet_module() {
$args = array(
'label' => __('Annonces'),
'singular_label' => __('Annonce'),
'public' => true,
'show_ui' => true,
'_builtin' => false, // It's a custom post type, not built in
'_edit_link' => 'post.php?post=%d',
'capability_type' => 'post',
'hierarchical' => false,
'rewrite' => array("slug" => "projects"),
'query_var' => "annonces", // This goes to the WP_Query schema
'supports' => array('title', 'editor', 'thumbnail') //titre + zone de texte + champs personnalisés + miniature valeur possible : 'title','editor','author','thumbnail','excerpt'
);
register_post_type( 'projet' , $args ); // enregistrement de l'entité projet basé sur les arguments ci-dessus
register_taxonomy_for_object_type('post_tag', 'projet','show_tagcloud=1&hierarchical=true'); // ajout des mots clés pour notre custom post type
//add_action("admin_init", "admin_init"); function pour ajouter des champs personnalisés
//add_action('save_post', 'save_custom'); function pour la sauvegarde de nos champs personnalisés
}
add_action('init', 'projet_module');






function admin_init(){ //initialisation des champs spécifiques
add_meta_box("Prix", "Prix", "Prix", "projet", "normal", "low");  //il s'agit de notre champ personnalisé qui apelera la fonction url_projet()
}
function Prix(){     //La fonction qui affiche notre champs personnalisé dans l'administration
global $post;
$custom = get_post_custom($post->ID); //fonction pour récupérer la valeur de notre champ
$url_projet = $custom["Prix"][0];
?>
<input size="70" type="text" value="<?php echo $Prix;?>" name="Prix"/>
<?php
}
function save_custom(){ //sauvegarde des champs spécifiques
global $post;
if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { // fonction pour éviter  le vidage des champs personnalisés lors de la sauvegarde automatique
return $postID;
}
update_post_meta($post->ID, "Prix", $_POST["Prix"]); //enregistrement dans la base de données
}


add_action("admin_init", "admin_init"); //function pour ajouter des champs personnalisés
add_action('save_post', 'save_custom'); //function pour la sauvegarde de nos champs personnalisés

?>


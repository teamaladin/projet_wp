<?php 

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'annonce',
    array(
      'labels' => array(
        'name' => __( 'Annonces' ),
        'singular_name' => __( 'Annonce' )
      ),
      'public' => true
    )
  );
}






function admin_init(){ //initialisation des champs spécifiques
add_meta_box("Prix", "Prix", "Prix", "annonce", "normal", "low");  //il s'agit de notre champ personnalisé qui apelera la fonction url_projet()
}
function Prix(){     //La fonction qui affiche notre champs personnalisé dans l'administration
global $post;
$custom = get_post_custom($post->ID); //fonction pour récupérer la valeur de notre champ
$url_projet = $custom["Prix"][0];
?>
<input size="70" type="text" value="<?php echo $Prix;?>" name="Prix"/>
<?php
}
function save_custom(){ //sauvegarde des champs spécifiques
global $post;
if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { // fonction pour éviter  le vidage des champs personnalisés lors de la sauvegarde automatique
return $postID;
}
update_post_meta($post->ID, "Prix", $_POST["Prix"]); //enregistrement dans la base de données
}


add_action("admin_init", "admin_init"); //function pour ajouter des champs personnalisés
add_action('save_post', 'save_custom'); //function pour la sauvegarde de nos champs personnalisés

add_filter( 'pre_get_posts', 'my_get_posts' );
function my_get_posts( $query ) {
 if ( is_home() )
 $query->set( 'post_type', array( 'annonce', 'post' ) );
 return $query;
}

?>

